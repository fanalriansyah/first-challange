"use strict";
const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class Match extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      models.Match.User = models.Match.belongsTo(models.User, {
        foreignKey: "player_1_id",
      });
      models.Match.User = models.Match.belongsTo(models.User, {
        foreignKey: "player_2_id",
      });
    }
  }
  Match.init(
    {
      player_1_choice: DataTypes.STRING,
      player_2_choice: DataTypes.STRING,
      player_1_result: DataTypes.STRING,
      player_2_result: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "Match",
    }
  );
  return Match;
};
