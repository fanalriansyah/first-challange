"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Room extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      models.Room.User = models.Room.belongsTo(models.User, {
        foreignKey: "player_1_id",
      });
      models.Room.User = models.Room.belongsTo(models.User, {
        foreignKey: "player_2_id",
      });
    }

    static createRoom = ({ room_name }) => {
      return this.create({ room_name });
    };

    // static getPlayerHand = (room, choice) => {
    //   if (!room.player_1_choice) {
    //     return this.update(
    //       { player_1_choice: choice },
    //       { where: { id: this.id } }
    //     );
    //   }
    //   return this.update(
    //     { player_2_choice: choice },
    //     { where: { id: this.id } }
    //   );
    // };

    static roomAuthenticate = async (id) => {
      try {
        const room = await this.findByPk(id);

        if (!room) {
          return Promise.reject("ID Not Found!");
        }

        return Promise.resolve(room);
      } catch (error) {
        return Promise.reject(error);
      }
    };
  }
  Room.init(
    {
      room_name: DataTypes.STRING,
      player_1_id: DataTypes.INTEGER,
      player_2_id: DataTypes.INTEGER,
      player_1_choice: DataTypes.STRING,
      player_2_choice: DataTypes.STRING,
      result: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "Room",
    }
  );
  return Room;
};
