"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Leaderboard extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      models.Leaderboard.User = models.Leaderboard.belongsTo(models.User, {
        foreignKey: "user_leaderboard_id",
      });

      models.Leaderboard.Match = models.Leaderboard.belongsTo(models.Match, {
        foreignKey: "user_match_id",
      });
    }
  }
  Leaderboard.init(
    {
      user_win: DataTypes.INTEGER,
      user_lose: DataTypes.INTEGER,
      user_win_rate: DataTypes.FLOAT,
    },
    {
      sequelize,
      modelName: "Leaderboard",
    }
  );
  return Leaderboard;
};
