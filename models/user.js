"use strict";
const { Model } = require("sequelize");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {}

    static #encrypt = (password) => bcrypt.hashSync(password, 10);

    generateToken = () => {
      const payLoad = {
        id: this.id,
        username: this.username,
      };

      const rahasia = "Shiba To The MOON";

      const token = jwt.sign(payLoad, rahasia);
      return token;
    };

    checkPassword = (password) => bcrypt.compareSync(password, this.password);

    static register = ({
      username,
      email,
      password,
      first_name,
      last_name,
      sex,
      dob,
      address,
      role,
    }) => {
      const encryptedPassword = this.#encrypt(password);
      const userRole = role !== "admin" ? "player" : "admin";

      return this.create({
        username,
        email,
        password: encryptedPassword,
        first_name,
        last_name,
        sex,
        dob,
        address,
        role: userRole,
      });
    };

    static adminAuthenticate = async ({ username, password }) => {
      try {
        const user = await this.findOne({ where: { username } });

        if (!user || !user.checkPassword(password)) {
          return Promise.reject("Invalid Username & Password !!");
        }
        if (user.role !== "admin") {
          return Promise.reject("Not Admin");
        }

        return Promise.resolve(user);
      } catch (error) {
        return Promise.reject(error);
      }
    };

    static userAuthenticate = async ({ username, password }) => {
      try {
        const user = await this.findOne({ where: { username } });
        if (user.role !== "player") {
          return Promise.reject("Youre not Player");
        }

        if (!user || !user.checkPassword(password)) {
          return Promise.reject("Invalid Username & Password !!");
        }

        return Promise.resolve(user);
      } catch (error) {
        return Promise.reject(error);
      }
    };
  }
  User.init(
    {
      username: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      first_name: DataTypes.STRING,
      last_name: DataTypes.STRING,
      sex: DataTypes.INTEGER,
      dob: DataTypes.DATE,
      address: DataTypes.TEXT,
      role: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "User",
    }
  );
  return User;
};
