const express = require("express");
const room = express.Router();
const roomController = require("../controllers/roomController");
const restrictJwt = require("../middlewares/restrict-jwt");

room.post("/", restrictJwt, roomController.createRoom);
room.get("/:id", restrictJwt, roomController.getRoomDetails);

module.exports = room;
