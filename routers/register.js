const express = require("express");
const register = express.Router();
const authController = require("../controllers/authController");

register.get("/", (req, res) => res.render("register"));
register.post("/", authController.registerAction);

module.exports = register;
