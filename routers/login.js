const express = require("express");
const login = express.Router();
const authController = require("../controllers/authController");

login.post("/", authController.loginAction);
login.get("/", (req, res) => res.render("login"));

module.exports = login;
