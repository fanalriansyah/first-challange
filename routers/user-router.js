const express = require("express");
const userRouter = express.Router();
const { User } = require("../models");

userRouter.get("/", (req, res) => {
  User.findAll().then((users) => {
    res.render("users", { users });
  });
});

userRouter.get("/register", (req, res) => {
  res.render("register");
});

userRouter.post("/", (req, res) => {
  User.create({
    username: req.body.username,
    email: req.body.email,
    password: req.body.password,
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    sex: req.body.sex,
    dob: req.body.dob,
    address: req.body.address,
  }).then(() => {
    res.redirect("/dashboard/user");
  });
});

userRouter.get("/detail/:id", (req, res) => {
  User.findOne({
    where: { id: req.params.id },
  }).then((user) => {
    if (!user) {
      res.status(404).send("Not found");
      return;
    }

    res.render("detailUser", { user });
  });
});

userRouter.get("/update/:id", (req, res) => {
  User.findOne({
    where: { id: req.params.id },
  }).then((user) => {
    if (!user) {
      res.status(404).send("Not found");
      return;
    }

    res.render("updateUser", { user });
  });
});

userRouter.post("/update", (req, res) => {
  User.update(
    {
      username: req.body.username,
      email: req.body.email,
      password: req.body.password,
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      sex: req.body.sex,
      dob: req.body.dob,
      address: req.body.address,
    },
    {
      where: { id: req.body.id },
    }
  ).then(() => {
    res.redirect("/dashboard/user");
  });
});

userRouter.get("/delete/:id", (req, res) => {
  User.destroy({
    where: { id: req.params.id },
  }).then(() => res.redirect("/dashboard/user"));
});

module.exports = userRouter;
