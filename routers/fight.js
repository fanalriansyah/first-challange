const express = require("express");
const fight = express.Router();
const fightController = require("../controllers/fightController");
const restrictJwt = require("../middlewares/restrict-jwt");

fight.get("/:id", restrictJwt, fightController.fightAction);
fight.post("/:id", restrictJwt, fightController.fightAction);

module.exports = fight;
