const express = require("express");
const apiAuth = express.Router();
const restrictJwt = require("../../middlewares/restrict-jwt");

const authController = require("../../controllers/api/authController");

apiAuth.post("/register", authController.registerAction);
apiAuth.post("/login", authController.loginAction);

apiAuth.get("/profile", restrictJwt, authController.profileAction);

module.exports = apiAuth;
