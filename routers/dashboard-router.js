const express = require("express");
const dashboardRouter = express.Router();
const userRouter = require("./user-router");
const { Match } = require("../models");
const { Leaderboard } = require("../models");
const restrict = require("../middlewares/restrict");

dashboardRouter.get("/", restrict, (req, res) => {
  res.render("dashboard");
});

dashboardRouter.get("/match", restrict, (req, res) => {
  Match.findAll().then((matches) => res.render("match", { matches }));
});

dashboardRouter.get("/leaderboard", restrict, (req, res) => {
  Leaderboard.findAll().then((leaderboards) =>
    res.render("leaderboard", { leaderboards })
  );
});

dashboardRouter.use("/user", restrict, userRouter);

module.exports = dashboardRouter;
