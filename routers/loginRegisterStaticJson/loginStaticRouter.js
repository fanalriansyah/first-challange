const express = require("express");
const loginStaticRouter = express.Router();
const users = require("../../users.json");

loginStaticRouter.post("/", (req, res) => {
  const { username, password } = req.body;
  const user = users.find((u) => u.username === username);
  const pass = users.find((u) => u.password === password);

  if (!username && !password) {
    res.status(401).send("Username or Password cannot be empty");
    return;
  }

  if (!user) {
    res.status(401).send(`Username is Inccorrect`);
    return;
  }
  if (!pass) {
    res.status(401).send(`Password is Inccorrect`);
  }
  res.status(200).json(user);
});

module.exports = loginStaticRouter;
