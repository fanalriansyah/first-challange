const usersRouter = require("express").Router();
const usersController = require("../../controllers/controllerStaticJson/usersController");

usersRouter.get("/", usersController.getUsers);

usersRouter.get("/:username", usersController.getUserbyUsername);

module.exports = usersRouter;
