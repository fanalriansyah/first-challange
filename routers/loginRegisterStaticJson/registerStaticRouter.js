const express = require("express");
const registerStaticRouter = express.Router();
const users = require("../../users.json");

registerStaticRouter.post("/", (req, res) => {
  const { username, password, address, phone } = req.body;
  const findUser = users.find((u) => u.username === username);

  if (!username || !password) {
    res.status(401).send("Username or Password cannot be empty");
    return;
  }
  if (findUser) {
    res.status(401).send("Username has been register");
    return;
  }

  const id = users[users.length - 1].id + 1;
  const user = { id, username, password, address, phone };
  users.push(user);
  res.status(201).json(user);
});

module.exports = registerStaticRouter;
