const { User } = require("../models");
const passport = require("passport");

// Register Lewat Template Engine
// const registerAction = (req, res, next) => {
//   User.register(req.body)
//     .then(() => res.redirect("/login"))
//     .catch((err) => next(err));
// };

// REGISTER from API
const registerAction = (req, res, next) => {
  User.register(req.body)
    .then(() => res.send("Register berhasil, Silahkan Login"))
    .catch(() => res.send("Periksa kembali data anda"));
};

const loginAction = passport.authenticate("local", {
  successRedirect: "/dashboard",
  failureRedirect: "/login",
  failureFlash: true,
});

module.exports = { registerAction, loginAction };
