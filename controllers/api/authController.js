const { User } = require("../../models");

const registerAction = (req, res, next) => {
  User.authenticate(req.body)
    .then((user) => {
      res.json({
        id: user.id,
        username: user.username,
      });
    })
    .catch((err) => next(err));
};

const loginAction = (req, res, next) => {
  User.userAuthenticate(req.body)
    .then((user) => {
      res.json({
        id: user.id,
        username: user.username,
        Token: user.generateToken(),
      });
    })
    .catch((err) => next(err));
};

const profileAction = (req, res, next) => {
  res.send(`Selamat ${req.user.username} Token anda Valid`);
};

module.exports = { registerAction, loginAction, profileAction };
