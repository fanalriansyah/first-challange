const { Room } = require("../models");

const createRoom = (req, res, next) => {
  Room.createRoom(req.body)
    .then((room) => {
      res.json({
        room_id: room.id,
        room_name: room.room_name,
      });
    })
    .catch((err) => res.send(err));
};

const getRoomDetails = (req, res, next) => {
  Room.findOne({
    where: { id: req.params.id },
  })
    .then((room) => res.json(room))
    .catch((err) => res.send(err));
};

module.exports = { createRoom, getRoomDetails };
