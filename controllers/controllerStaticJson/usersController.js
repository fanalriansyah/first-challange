const users = require("../../users.json");

const getUsers = (req, res) => {
  res.status(200).json(users);
};

const getUserbyUsername = (req, res) => {
  const user = users.find((u) => u.username === req.params.username);

  if (!user) {
    res.status(404).json({});
    return;
  }
  res.status(200).json(user);
};

module.exports = {
  getUsers,
  getUserbyUsername,
};
