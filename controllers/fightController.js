const { Room } = require("../models");
const rps = require("../lib/rps");

const fightAction = (req, res, next) => {
  const playerHand = req.body.hand;

  if (!playerHand) {
    return res.status(400).send("Hand Cannot Be Empty");
  }

  Room.findOne({
    where: { id: req.params.id },
  })
    .then((room) => {
      if (!room.player_1_choice) {
        return Room.update(
          {
            player_1_id: req.user.id,
            player_1_choice: playerHand,
          },
          {
            where: { id: req.params.id },
          }
        )
          .then(() => {
            res.json(room);
          })
          .catch((err) => next(err));
      }
      if (!room.player_2_choice) {
        const result = rps(room, playerHand);
        return Room.update(
          {
            player_2_id: req.user.id,
            player_2_choice: playerHand,
            result: result,
          },
          {
            where: { id: req.params.id },
          }
        )
          .then(() => res.json(room))
          .catch((err) => next(err));
      }
      res.status(400).send("Room Is Full");
    })
    .catch((err) => next(err));
};

module.exports = { fightAction };
