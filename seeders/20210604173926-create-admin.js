"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.bulkInsert("Users", [
      {
        username: "admin",
        email: "admin@gmail.com",
        password: "admin",
        first_name: "ad",
        last_name: "min",
        sex: "0",
        dob: "2012-12-12",
        address: "Unknown",
        createdAt: new Date(),
        updatedAt: new Date(),
        role: "admin",
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    queryInterface.bulkDelete("Users", null, {});
  },
};
