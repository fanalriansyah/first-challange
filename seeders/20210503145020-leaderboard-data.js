"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("Leaderboards", [
      {
        user_win: 1,
        user_lose: 0,
        user_win_rate: 9.8,
        createdAt: new Date(),
        updatedAt: new Date(),
        user_leaderboard_id: 2,
        user_match_id: 1,
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Leaderboards", null, {});
  },
};
