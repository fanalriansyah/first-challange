"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("Users", [
      {
        username: "opanpan",
        email: "opanpangmail.com",
        password: "opanpan123",
        first_name: "opan",
        last_name: "pan",
        sex: 1,
        dob: "2016-10-10",
        address: "Bukit Pamulang Indah",
        createdAt: new Date(),
        updatedAt: new Date(),
        role: "player",
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Users", null, {});
  },
};
