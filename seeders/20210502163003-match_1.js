"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("Matches", [
      {
        player_1_choice: "rock",
        player_2_choice: "paper",
        player_1_result: "win",
        player_2_result: "lose",
        createdAt: new Date(),
        updatedAt: new Date(),
        player_1_id: 2,
        player_2_id: 3,
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Marches", null, {});
  },
};
