"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("Users", [
      {
        username: "dheadhei",
        email: "dheadhei@gmail.com",
        password: "dheadhei123",
        first_name: "dhea",
        last_name: "dhei",
        sex: 0,
        dob: "2016-11-11",
        address: "Duren Sawit",
        createdAt: new Date(),
        updatedAt: new Date(),
        role: "player",
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Users", null, {});
  },
};
