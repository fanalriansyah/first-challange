"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.addColumn("Leaderboards", "user_leaderboard_id", {
      type: Sequelize.INTEGER,
      references: {
        model: "Users",
        key: "id",
        onUpdate: "cascade",
        onDelete: "cascade",
      },
    });
  },

  down: async (queryInterface, Sequelize) => {
    queryInterface.removeColumn("Leaderboards", "user_leaderboard_id");
  },
};
