"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("Users", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      username: {
        allowNull: false,
        unique: true,
        notEmpty: true,
        type: Sequelize.STRING,
      },
      email: {
        allowNull: false,
        isEmail: true,
        unique: true,
        type: Sequelize.STRING,
      },
      password: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      first_name: {
        allowNull: false,
        notEmpty: true,
        type: Sequelize.STRING,
      },
      last_name: {
        allowNull: false,
        notEmpty: true,
        type: Sequelize.STRING,
      },
      sex: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      dob: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      address: {
        type: Sequelize.TEXT,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("Users");
  },
};
