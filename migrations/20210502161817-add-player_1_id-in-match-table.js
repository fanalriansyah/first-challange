"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.addColumn("Matches", "player_1_id", {
      type: Sequelize.INTEGER,
      references: {
        model: "Users",
        key: "id",
        onUpdate: "cascade",
        onDelete: "cascade",
      },
    });
    queryInterface.addColumn("Matches", "player_2_id", {
      type: Sequelize.INTEGER,
      references: {
        model: "Users",
        key: "id",
        onUpdate: "cascade",
        onDelete: "cascade",
      },
    });
  },

  down: async (queryInterface, Sequelize) => {
    queryInterface.removeColumn("Matches", "player_1_id");
    queryInterface.removeColumn("Matches", "player_2_id");
  },
};
