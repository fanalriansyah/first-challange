"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.addColumn("Leaderboards", "user_match_id", {
      type: Sequelize.INTEGER,
      references: {
        model: "Matches",
        key: "id",
        onUpdate: "cascade",
        onDelete: "cascade",
      },
    });
  },

  down: async (queryInterface, Sequelize) => {
    queryInterface.removeColumn("Leaderboards", "user_match_id");
  },
};
