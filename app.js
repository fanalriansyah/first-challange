const express = require("express");
const app = express();
const PORT = 3001;
const router = require("./router");
const session = require("express-session");
const flash = require("express-flash");
const passport = require("./lib/passport");
const passportJwt = require("./lib/passport-jwt");

app.use(express.urlencoded({ extended: false }));
app.use(
  session({
    secret: "secret",
    resave: false,
    saveUninitialized: false,
  })
);
app.use(passportJwt.initialize());
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use(express.static("assets"));
app.use(express.json());

app.set("view engine", "ejs");

app.use(router);

app.listen(PORT, () => console.log(`Running at http://localhost:${PORT}`));
