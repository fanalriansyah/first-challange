const router = require("express").Router();
const dashboardRouter = require("./routers/dashboard-router");
const usersRouter = require("./routers/loginRegisterStaticJson/usersStatic");
const loginRouterStatic = require("./routers/loginRegisterStaticJson/loginStaticRouter");
const registerRouterStatic = require("./routers/loginRegisterStaticJson/registerStaticRouter");
const loginRouter = require("./routers/login");
const registerRouter = require("./routers/register");
const apiAuthRouter = require("./routers/api/auth");
const roomRouter = require("./routers/room");
const fightRouter = require("./routers/fight");

router.get("/", (req, res) => res.render("index"));
router.get("/games", (req, res) => res.render("games"));

router.use("/loginStatic", loginRouterStatic);
router.use("/registerStatic", registerRouterStatic);

router.use("/users", usersRouter);

router.use("/dashboard", dashboardRouter);

router.use("/login", loginRouter);
router.use("/register", registerRouter);

router.use("/api", apiAuthRouter);

router.use("/room", roomRouter);

router.use("/fight", fightRouter);

module.exports = router;
