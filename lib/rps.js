const getResultRps = (room, hand) => {
  if (room.player_1_choice === hand) {
    return "Draw";
  }
  if (room.player_1_choice === "rock") {
    return hand === "scissors" ? "Player 1 Win" : "Player 2 Win";
  }
  if (room.player_1_choice === "paper") {
    return hand === "rock" ? "Player 1 Win" : "Player 2 Win";
  }
  if (room.player_1_choice === "scissors") {
    return hand === "paper" ? "Player 1 Win" : "Player 2 Win";
  }
};

module.exports = getResultRps;
